/**
 * Created by igormoskal on 5/5/16.
 */

(function ($) {
    'use strict';


    $("#comment-form input, #comment-form textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
           console.log('error');
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: new FormData($form[0]),
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(json) {
                    // Success message
                    if(json.status !== 'ok'){
                        for (var i in json['error']) {
                            var element = $('#' + i);
                            if (element.parent().hasClass('form-group')) {
                                element.parents('.form-group').addClass('has-error');
                                element.next().html('<ul role="alert"><li>' + json['error'][i] + '</li></ul>');
                            }
                        }
                        console.log('error');
                    } else {
                        $('#success').html("<div class='alert alert-success'>");
                        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-success')
                            .append("<strong>Your comment has been sent. </strong>");
                        $('#success > .alert-success')
                            .append('</div>');

                        //clear all fields
                        $form.trigger("reset");
                    }
                },
                error: function() {

                }
            })
        },
        filter: function() {
            return $(this).is(":visible");
        }
    });

    $("#previewModal").on("show.bs.modal", function(e) {
        var $form = $("#comment-form");
        var $this = $(this);
        if ($("#comment-form input, #comment-form textarea").jqBootstrapValidation("hasErrors")) {
            var target = e.relatedTarget;
            $(target).after('<p class="has-error"><span class="control-label">Complite all fields </span> </p>');
            return false;
        }
        var link = $(e.relatedTarget);
        console.log(link.attr("href"));
        $.ajax({
            type: 'post',
            url: link.attr("href"),
            data: new FormData($form[0]),
            processData: false,
            contentType: false,
            success: function(html) {
                console.log(html);
                console.log($this);
                $this.find(".modal-body").html(html);
            },
            error: function() {

            }
        });
    });

    $(".status:checkbox").change(function() {
        var status;
        if($(this).is(":checked")) {
            status = 1;
        } else {
            status = 0;
        }
        $.ajax({
            url: '/admin/comment/status',
            type: 'POST',
            data: { id:$(this).data('id'), status: status }
        });
    });
})(window.jQuery);
