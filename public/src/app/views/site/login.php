<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */
?>
<?php $this->render('layouts/header')?>
<div class="container">
<div class="site-login">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <p>Please fill out the following fields to login:</p>
            <div class="account-wall">
                <img class="profile-img" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
                     alt="">
                <form method="post" action="login" class="form-signin">
                    <div class="control-group form-group">
                        <div class="controls">
                            <input type="text" name="login" class="form-control" placeholder="Email" required autofocus>
                            </div>
                        </div>
                    <div class="control-group form-group">
                        <div class="controls">
                            <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">
                        Sign in</button>
                </form>
            </div>
            <div class="col-lg-offset-1" style="color:#999;">
                You may login with <strong><?php echo $this->loginData['login']?>/<?php echo $this->loginData['password']?></strong>
            </div>
        </div>
    </div>
</div>
<?php $this->render('layouts/footer')?>

