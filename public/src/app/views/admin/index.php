<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */
?>
<?php $this->render('layouts/header')?>
<?php if($comments) : ?>
<div class="container">
    <table class="table table-striped table-bordered"><thead>
        <tr>
            <th> ID </th>
            <th> Image </th>
            <th>Author</th>
            <th>Email</th>
            <th>Subject</th>
            <th>Text</th>
            <th>Status</th>
            <th>Add Date</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($comments as $comment) : ?>
        <tr data-key="<?php echo $comment['id']?>"><td><?php echo $comment['id']?></td>
            <td style="width: 140px; height: 80px"> <img class="img-responsive" src="<?php echo $comment['image']?>" alt="<?php echo $comment['subject']?>"> </td>
            <td><?php echo $comment['author']?></td>
            <td><?php echo $comment['email']?></td>
            <td><?php echo $comment['subject']?></td>
            <td><?php echo $comment['text']?></td>
            <td>
                <input data-id="<?php echo $comment['id']?>" name="status" class="status" value="1" <?php echo $comment['status'] ? " checked='checked' " : ''?> type="checkbox">
            </td>
            <td><?php echo $comment['date_add']?></td>
            <td>
                <a href="/admin/comment/edit/<?php echo $comment['id']?>" title="Edit" aria-label="Edit" data-pjax="0">
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
            </td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php else:?>
        <p> No Comments </p>
    <?php endif;?>
<?php $this->render('layouts/footer')?>
