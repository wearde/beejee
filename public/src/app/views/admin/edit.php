<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */
?>
<?php $this->render('layouts/header')?>
<div class="container">

<form  name="sentMessage" id="comment-form-admin" action="/admin/comment/edit/<?php echo $comment['id']?>" method="post" enctype="multipart/form-data" novalidate="">
    <input type="hidden" name="id" value="<?php echo $comment['id']?>">
    <div class="control-group form-group">
        <div class="controls">
            <label>Full Name:</label>
            <input type="text" name="name" minlength="3" maxlength="64" class="form-control" id="name" value="<?php echo $comment['author']?>" required="" data-validation-required-message="Please enter your name." aria-invalid="false">
            <p class="help-block"></p>
        </div>
    </div>

    <div class="control-group form-group">
        <div class="controls">
            <label>Email Address:</label>
            <input type="email" name="email" required class="form-control" value="<?php echo $comment['email']?>" id="email">
            <div class="help-block"></div></div>
    </div>
    <div class="control-group form-group">
        <div class="controls">
            <label>Subject:</label>
            <input type="text" name="subject" minlength="3" maxlength="64" required class="form-control" value="<?php echo $comment['subject']?>" id="subject">
            <div class="help-block"></div>
        </div>
    </div>
    <div class="control-group form-group">
        <div class="controls">
            <label>Message:</label>
            <textarea rows="10" name="text" minlength="10" cols="100" required  class="form-control" id="text"><?php echo $comment['text']?></textarea>
            <div class="help-block"></div>
        </div>
    </div>
    <div id="success"></div>
    <!-- For success/fail messages -->
    <button type="submit" class="btn btn-primary">Send Message</button>
</form>
<?php $this->render('layouts/footer')?>