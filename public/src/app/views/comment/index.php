<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */
?>
<?php $this->render('layouts/header')?>
<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Page Heading
                <small>Secondary Text</small>
            </h1>
        </div>
        <div class="col-sm-12">
            <!-- Split button -->
            <div class="btn-group">
                <button type="button" class="btn btn-default">Order by added Date</button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="/comments/author/asc">Order by Author Name</a></li>
                    <li><a href="/comments/email/asc">Order by Author Email</a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php if($comments) : ?>
        <?php foreach($comments as $comment): ?>
            <div class="row">

                <!-- Blog Entries Column -->
                <div class="col-md-12">
                    <!-- Post -->
                    <h2>
                        <span ><?php echo $comment['subject']?></span>
                    </h2>
                    <p class="lead">
                        by <span><?php echo $comment['author']?></span>
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo date('l dS F Y H:i', strtotime($comment['date_add']))?></p>
                    <hr>
                    <div class="row">
                        <div class="col-sm-4">
                            <img class="img-responsive" src="/<?php echo $comment['image']?>" alt="<?php echo $comment['subject']?>">
                        </div>
                        <div class="col-sm-8">
                            <p><?php echo $comment['text']?></p>
                            <?php if($comment['is_admin']): ?>
                                <p class="text-info">
                                    Your comment was editing by Admin
                                </p>
                            <?php endif;?>
                        </div>
                    </div>

                </div>
            </div>
            <hr>
        <?php endforeach;?>
    <?php endif;?>
    <hr>
    <div class="row">
        <div class="col-md-8">
            <h3>Send us a Message</h3>
            <form role="form" data-toggle="validator" name="sentMessage" id="comment-form" action="comments/add" method="post" enctype="multipart/form-data" novalidate="">
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Full Name:</label>
                        <input type="text" name="name" minlength="3" maxlength="64" class="form-control" id="name" value="" required="" data-validation-required-message="Please enter your name." aria-invalid="false">
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="control-group form-group">
                    <div class="controls">
                        <label>Email Address:</label>
                        <input type="email" name="email" required class="form-control" value="" id="email">
                        <div class="help-block"></div></div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Subject:</label>
                        <input type="text" name="subject" minlength="3" maxlength="64" required class="form-control" value="" id="subject">
                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="control-group form-group">
                    <div class="controls">
                        <label>Message:</label>
                        <textarea rows="10" name="text" minlength="10" cols="100" required  class="form-control" id="text"></textarea>
                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="control-group form-group">
                    <label for="file">Add Image</label>
                    <input type="file" name="file" required="required" value="" id="file">
                    <div class="help-block"></div>
                </div>
                <div id="success"></div>
                <!-- For success/fail messages -->
                <button type="submit" class="btn btn-primary">Send Message</button>
                <a href="/comments/preview" data-remote="false" data-toggle="modal" data-target="#previewModal" class="btn btn-default">
                    Life Preview
                </a>
            </form>
        </div>
        <div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="previewModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->
    <?php $this->render('layouts/footer')?>