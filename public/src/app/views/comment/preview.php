<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */
?>
<div class="row">

    <!-- Blog Entries Column -->
    <div class="col-md-12">
        <?php if($data): ?>
        <!-- Post -->
        <h2>
            <span ><?php echo $data['subject']?></span>
        </h2>
        <p class="lead">
            by <span><?php echo $data['name']?></span>
        </p>
        <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo date('l dS F Y H:i', time())?></p>
        <hr>
        <div class="row">
            <div class="col-sm-4">
                <img class="img-responsive" src="<?php echo $data['image']?>" alt="">
            </div>
            <div class="col-sm-8">
                <p><?php echo  $data['text']?></p>

            </div>
        </div>
        <?php else: ?>
        <p> Not data to display </p>
        <?php endif;?>
    </div>
</div>
