<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\app\controllers;

use amass\core\base\Controller;

class SiteController extends Controller
{
    public function actionIndex(){
        echo 'siteIndex';
    }
    
    public function actionLogin(){
        if($this->request->server['REQUEST_METHOD'] == 'POST' && $this->login($this->request->post))
            $this->response->redirect('admin');
        $this->render('site/login');
    }

    public function actionLogout(){
        unset($this->session->data['uId']);
        unset($this->session->data['login']);
        $this->response->redirect('comments');
    }
    
    public function actionError(){
        echo 'siteError';
    }

    /**
     * @param $data
     * @return bool
     */
    private function login($data){
        if($data['login'] == $this->loginData['login'] && $data['password'] == $this->loginData['password'])
        {
            $this->session->data['login'] = true;
            $this->session->data['uId'] = $this->loginData['uId'];
            return true;
        } else {
            return false;
        }
    }
}