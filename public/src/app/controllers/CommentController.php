<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\app\controllers;

use amass\app\models\Comment;
use amass\core\base\Controller;
use amass\core\web\Files;
use Eventviva\ImageResize;

class CommentController extends Controller
{
    /**
     * @var array
     */
    private $_error = [];
    
    
    public function actionIndex($sort = 'date', $order = 'desc'){
        $data = [
            'sort' => $sort,
            'order' => $order
        ] ;
        $model = (new Comment())->getComments($data);
        $this->render('comment/index', [
            'comments' => $model
        ]);
    }

    public function actionPreview(){
        $imageObject = null;
        $image = '';
        $data = $this->request->post;
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            if($this->request->files && isset($this->request->files['file']))
            {

                /**
                 * @var $imageObject ImageResize
                 */
                $imageObject = (new Files($this->request->files['file']))->checkType();

                if(!empty($imageObject))
                {
                    $image =  'images/' . uniqid() . '_' .$this->request->files['file']['name'];
                    $imageObject->save($this->request->server['DOCUMENT_ROOT']. '/' .$image);
                }
            }
        }
       $data['image'] = $image;
        $this->render('comment/preview', [
            'data' => $data
        ]);
    }

    public function actionAdd(){
        $json = [
            'status' => 'false',
            'error'  => []
        ];
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $imageObject = null;
            $image = '';

            if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->addValidate()) {

                if($this->request->files && isset($this->request->files['file']))
                {
                    /**
                     * @var $imageObject ImageResize
                     */
                    $imageObject = (new Files($this->request->files['file']))->checkType();

                    if(empty($imageObject))
                    {
                        $json['error']['file'] = 'Allowed type of files (png, jpg, gif)';
                    } else{
                        $image =  'images/' . uniqid() . '_' .$this->request->files['file']['name'];
                    }
                }
               if($image){
                    $model = (new Comment())->addComment($this->request->post, $image);
                    if($model){
                        if($imageObject)
                            $imageObject->save($this->request->server['DOCUMENT_ROOT']. '/' .$image);

                        $json['status'] = 'ok';
                    }
                }
            }

            $json['error'] = array_merge_recursive($json['error'], $this->_error);

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json))->output();
        }
    }

    /**
     * 
     */
    public function addValidate(){
        if ((mb_strlen($this->request->post['name']) < 3) || (mb_strlen($this->request->post['name']) > 64)) {
            $this->_error['name'] = 'Name Required (more 3 & less 64)';
        }

        if ((mb_strlen($this->request->post['subject']) < 3) || (mb_strlen($this->request->post['subject']) > 64)) {
            $this->_error['subject'] = 'Subject Required (more 3 & less 64)';
        }

        if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
            $this->_error['email'] = 'Email Required & not validate';
        }

        if ((mb_strlen($this->request->post['text']) < 10)) {
            $this->_error['text'] = 'Text Required (more than 10)';
        }

        return !$this->_error;
    }
}