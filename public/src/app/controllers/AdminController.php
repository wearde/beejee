<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\app\controllers;

use amass\app\components\AuthController;
use amass\app\models\Comment;

class AdminController extends AuthController
{
    private $_error;
    public function actionIndex(){

        if($this->isGuest)
            $this->response->redirect('login');

        $model = (new Comment())->allComments();

        $this->render('admin/index', [
            'comments' => $model
        ]);
    }

    public function actionEdit($id){
        if($this->isGuest)
            $this->response->redirect('/login');

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->addValidate()) {
            (new Comment())->updateComment($this->request->post['id'], $this->request->post);
            $this->response->redirect('/admin');
        }
        
        $model = (new Comment())->editComment($id);

        $this->render('admin/edit', [
            'comment' => $model,
            'error' => $this->_error
        ]);
    }


    public function actionStatus(){
        if($this->isGuest)
            return false;
        if($this->request->post)
        {
            (new Comment())->toggleStatus($this->request->post);
        }
    }

    /**
     *
     */
    public function addValidate(){
        if ((mb_strlen($this->request->post['name']) < 3) || (mb_strlen($this->request->post['name']) > 64)) {
            $this->_error['error_name'] = 'Name Required (more 3 & less 64)';
        }

        if ((mb_strlen($this->request->post['subject']) < 3) || (mb_strlen($this->request->post['subject']) > 64)) {
            $this->_error['error_subject'] = 'Subject Required (more 3 & less 64)';
        }

        if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
            $this->_error['error_email'] = 'Email Required & not validate';
        }

        if ((mb_strlen($this->request->post['text']) < 10)) {
            $this->_error['error_text'] = 'Text Required (more than 10)';
        }

        return !$this->_error;
    }
}