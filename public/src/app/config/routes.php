<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

return [
    'login' => 'site/login',
    'logout' => 'site/logout',
    'admin/comment/edit/([0-9]+)' => 'admin/edit/$1',
    'admin/comment/status' => 'admin/status',
    'admin' => 'admin/index',
    'comments/([\w_\/-]+)/([a-z]+)' => 'comment/index/$1/$2',
    'comments/([0-9]+)' => 'comment/view/$1',
    'comments/edit/([0-9]+)' => 'comment/edit/$1',
    'comments/preview' => 'comment/preview',
    'comments/add' => 'comment/add',
    'comments' => 'comment/index',
    '' => 'comment/index'
];