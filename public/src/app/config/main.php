<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

$rules = require(__DIR__ . '/routes.php');
$db = require(__DIR__ . '/db.php');
$config = [
    'id' => 'basic',
    'namespace' => 'amass\\app\\',
    'defaultController' => 'CommentController',
    'defaultAction' => 'index',
    'errorAction' => 'site/error',
    'basePath' => dirname(__DIR__),
    'user' => [
        'uId' => '123456',
        'login' => 'admin',
        'password'  => '123'
    ],
    'db' => $db,
    'rules' => $rules
];

return $config;