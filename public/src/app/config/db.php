<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

return [    
        'dsn' => 'mysql:host=localhost;dbname=beejee;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock',
        'username' => 'root',
        'password' => '1',
        'charset' => 'utf8',
];