<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\app\models;

use amass\core\base\Model;
use amass\core\base\Query;

class Comment extends Model
{
    /**
     * @return array|int|null
     */
    public function allComments(){
        return (new Query())->query('SELECT * FROM comments');
    }

    /**
     * @param array $data
     * @return array|int|null
     */
    public function getComments($data = [])
    {        
        $sort_data = array(
            'author',
            'email',
            'date_add'
        );
        
        $sql = 'SELECT * FROM comments WHERE status=:status';

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            if ($data['sort'] == 'author' || $data['sort'] == 'author') {
                $sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
            } else {
                $sql .= " ORDER BY " . $data['sort'];
            }
        } else {
            $sql .= " ORDER BY date_add";
        }

        if (isset($data['order']) && ($data['order'] == 'asc')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }
        return (new Query())->query($sql, array('status' => 1));
    }

    /**
     * @param $data
     * @param $image
     * @return array|int|null
     */
    public function addComment($data, $image){
        return (new Query())->query("INSERT INTO comments(`subject`, `text`, `author`, `email`, `image`, `date_add`) 
                              VALUES(:subject, :text, :author, :email, :image, NOW())",
                array(
                    'subject'=> $data['subject'],
                    'text'   => $data['text'],
                    'author' => $data['name'],
                    'email'=> $data['email'],
                    'image'   => $image
                    )
        );
    }

    /**
     * @param $data
     * @return array|int|null
     */
    public function toggleStatus($data){
        return (new Query())->query("UPDATE comments SET status=:status WHERE id=:id",
            array(
                'id' => (int)$data['id'],
                'status' => (int)$data['status']
            )
        );
    }

    /**
     * @param $id
     * @return array|int|null
     */
    public function editComment($id){
        return (new Query())->queryOne('SELECT * FROM comments WHERE id=:id', array('id' => $id));
    }

    /**
     * @param $id
     * @param $data
     * @return array|int|null
     */
    public function updateComment($id, $data){
        return (new Query())->query("UPDATE comments SET subject=:subject, text=:text, author=:author, email=:email, date_add=NOW(), is_admin=1  
                                      WHERE  id=:id",
            array(
                'id' => $id,
                'subject'=> $data['subject'],
                'text'   => $data['text'],
                'author' => $data['name'],
                'email'=> $data['email'],
            )
        );
    }
}