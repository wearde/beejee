<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass;

/**
 * This constant defines the core directory.
 */
use amass\core\base\InvalidCallException;
use amass\core\base\Registry;
use amass\core\base\UnknownPropertyException;

defined('CORE_PATH') or define('CORE_PATH', __DIR__);

/**
 * This constant defines whether the application should be in debug mode or not. Defaults to false.
 */
defined('APP_DEBUG') or define('APP_DEBUG', false);

/**
 * Base is the core helper class.
 * @author igormoskal <wearde.studio@gmail.com>
 */
class Base
{
    /**
     * @var string
     */
    public $namespace = 'amass\\app\\';

    /**
     * @var Registry
     */
    private $registry;
    
    private $_viewDir = 'views';
    
    public static $app = [];

    /**
     * @var
     */
    private $_basePath;

    protected $config = [];

    private static $_instance; //The single instance
    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance() {
        if(!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function getRegistry(){
        return $this->registry;
    }
    
    public function setRegistry($registry){
        $this->registry = $registry;
    }
    /**
     * Constructor.
     *
     */
    public function __construct()
    {
    }

    /**
     * Returns the value of an object property.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$value = $object->property;`.
     * @param string $name the property name
     * @return mixed the property value
     * @throws UnknownPropertyException if the property is not defined
     * @throws InvalidCallException if the property is write-only
     * @see __set()
     */
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (method_exists($this, 'set' . $name)) {
            throw new InvalidCallException('Getting write-only property: ' . get_class($this) . '::' . $name);
        } else {
            throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }
    /**
     * Sets value of an object property.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$object->property = $value;`.
     * @param string $name the property name or the event name
     * @param mixed $value the property value
     * @throws UnknownPropertyException if the property is not defined
     * @throws InvalidCallException if the property is read-only
     * @see __get()
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } elseif (method_exists($this, 'get' . $name)) {
            throw new InvalidCallException('Setting read-only property: ' . get_class($this) . '::' . $name);
        } else {
            throw new UnknownPropertyException('Setting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    /**
     * Returns the fully qualified name of this class.
     * @return string the fully qualified name of this class.
     */
    public static function className()
    {
        return get_called_class();
    }

    /**
     * Returns the root directory of the module.
     * It defaults to the directory containing the module class file.
     * @return string the root directory of the module.
     */
    public function getBasePath()
    {
        if ($this->_basePath === null) {
            $class = new \ReflectionClass($this);
            $this->_basePath = dirname($class->getFileName());
        }

        return $this->_basePath;
    }

    /**
     * @param $_file_
     * @param array $_params_
     * @return mixed
     */
    public function renderFile($_file_, $_params_ = [])
    {
        ob_start();
        ob_implicit_flush(false);
        extract($_params_, EXTR_OVERWRITE);
        require($_file_);

        return ob_get_clean();
    }

    public function render($view, $params = [])
    {
        $viewFile = $this->findViewFile($view);
        echo  $this->renderFile($viewFile, $params);
    }

    public function findViewFile($view){
        //return $view;
        $file = dirname($this->getBasePath())  . DIRECTORY_SEPARATOR . $this->_viewDir . DIRECTORY_SEPARATOR .  $view . '.php';
        if (file_exists($file)) {
            return $file;
        } else {
            throw new \BadMethodCallException("Unable to resolve view file for view '$view': no active view context.");
        }

    }
}