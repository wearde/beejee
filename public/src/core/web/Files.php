<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\web;

use amass\Base;
use Eventviva\ImageResize;

class Files extends Base
{
    public $imageWidth = 340;
    public $imageHeight = 240;
    /**
     * @var array
     */
    private $_file;

    /**
     * @var array
     */
    public $allowedMimeTypes = [
        'image/gif',
        'image/png',
        'image/jpeg',
        'image/pjpeg'
    ];

    /**
     * Files constructor.
     * @param array $file
     */
    public function __construct($file = [])
    {
        $this->_file = $file;
        return $this;
    }

    /**
     * @return bool|string
     */
    public function checkType(){
        if(in_array($this->_file['type'], $this->allowedMimeTypes)){
           return $this->_getData();
        }
        return null;
    }

    private function _getData(){
        $image = new ImageResize($this->_file['tmp_name']);
        $image->resize($this->imageWidth, $this->imageHeight);
       return $image;
    }
}