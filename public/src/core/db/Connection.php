<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\db;

use amass\Base;

class Connection extends Base
{
    private $_connection;
    private static $_instance; //The single instance
    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance($config = []) {
        if(!self::$_instance) { // If no instance then make one
            self::$_instance = new self($config);
        }
        return self::$_instance;
    }

    /**
     * Connection constructor.
     * @param array $config
     */
    public function __construct($config) {
        parent::__construct();

        $dbConfig = $config['db'];
        try {
            $this->_connection = new \PDO($dbConfig['dsn'], $dbConfig['username'], $dbConfig['password']);
            
        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /**
     * Magic method clone is empty to prevent duplication of connection
     */
    private function __clone() { }

    /**
     * Get mysqli connection
     * @return \PDO
     */
    public function getConnection() {
        return $this->_connection;
    }
}