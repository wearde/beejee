<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\base;

use amass\Base;

class View extends Base
{
    public static function renderPartial($view, $params = []){
        self::getInstance()->render($view, $params);
    }
}