<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\base;

use amass\Base;
use amass\core\db\Connection;

class Application extends Base
{

    /**
     * Application constructor.
     * @param $config
     */
    public function __construct($config)
    {
        parent::__construct();
        /**
         * @todo make better
         */
        self::$app = $config;
        
        // Registry
        self::getInstance()->registry = new Registry();
        self::getInstance()->registry->set('route', new Route($config));
        self::getInstance()->registry->set('db', new Connection($config));
        self::getInstance()->registry->set('request', new Request());
        self::getInstance()->registry->set('response', new Response());
        self::getInstance()->registry->set('session', new Session());
    }
    
    /**
     * Main method Run Application 
     */
    public function run(){
        $route =  self::getInstance()->getRegistry()->get('route');
        $route->run();
    }
}