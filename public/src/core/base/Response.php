<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\base;

use amass\Base;

class Response  extends Base{
    /**
     * @var array
     */
    private $headers = array();

    /**
     * @var
     */
    private $output;

    /**
     * @param $header
     * @return $this
     */
    public function addHeader($header) {
        $this->headers[] = $header;
        return $this;
    }

    /**
     * @param $url
     * @param int $status
     */
    public function redirect($url, $status = 302) {
        header('Location: ' . str_replace(array('&amp;', "\n", "\r"), array('&', '', ''), $url), true, $status);
        exit();
    }

    /**
     * @param $output
     * @return $this
     */
    public function setOutput($output) {
        $this->output = $output;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutput() {
        return $this->output;
    }

    /**
     * 
     */
    public function output() {
        if ($this->output) {
            $output = $this->output;

            if (!headers_sent()) {
                foreach ($this->headers as $header) {
                    header($header, true);
                }
            }

            echo $output;
        }
    }
}