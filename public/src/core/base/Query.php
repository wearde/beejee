<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\base;

use amass\Base;

class Query extends Base
{
    /**
     * @var \PDO
     */
    protected $db;
    /**
     * @var \PDOStatement
     */
    private $sQuery;

    /**
     * @var
     */
    private $parameters;
    /**
     * @var int
     */
    public $rowCount = 0;
    /**
     * @var int
     */
    public $columnCount = 0;

    /**
     * @var int
     */
    public $queryCount = 0;

    /**
     * @var
     */
    public $success;

    /**
     * Query constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->db = self::getInstance()->getRegistry()->get('db')->getConnection();
        parent::__construct();

        return $this;
    }

    /**
     * @return \PDO
     */
    public function getQuery(){
        return $this->db;
    }

    /**
     * @param $query
     * @param array $parameters
     */
    private function init($query, $parameters = [])
    {
        try {
            $this->parameters = $parameters;
            $this->sQuery = $this->db->prepare($this->buildParams($query, $this->parameters));

            if (!empty($this->parameters)) {
                if (array_key_exists(0, $parameters)) {
                    $parametersType = true;
                    array_unshift($this->parameters, "");
                    unset($this->parameters[0]);
                } else {
                    $parametersType = false;
                }
                foreach ($this->parameters as $column => $value) {
                    $this->sQuery->bindParam($parametersType ? intval($column) : ":" . $column, $this->parameters[$column]); //It would be query after loop end(before 'sQuery->execute()').It is wrong to use $value.
                }
            }

            $this->success = $this->sQuery->execute();
            $this->queryCount++;
        }
        catch (\PDOException $e) {
            print_r($e);
            die();
        }
        $this->parameters = array();
    }

    /**
     * @param $query
     * @param null $params
     * @return mixed
     */
    private function buildParams($query, $params = null)
    {
        if (!empty($params)) {
            $rawStatement = explode(" ", $query);
            foreach ($rawStatement as $value) {
                if (strtolower($value) == 'in') {
                    return str_replace("(?)", "(" . implode(",", array_fill(0, count($params), "?")) . ")", $query);
                }
            }
        }
        return $query;
    }

    /**
     * @param $query
     * @param null $params
     * @param int $fetchMode
     * @param bool $all
     * @return array|int|null
     */
    public function query($query, $params = null, $fetchMode = \PDO::FETCH_ASSOC, $all = true)
    {
        $query = trim($query);
        $rawStatement = explode(" ", $query);
        $this->init($query, $params);
        $statement = strtolower($rawStatement[0]);
        if ($statement === 'select' || $statement === 'show') {
            if($all)
                return $this->sQuery->fetchAll($fetchMode);
            return $this->sQuery->fetch($fetchMode);
        } elseif ($statement === 'insert' || $statement === 'update' || $statement === 'delete') {
            return $this->sQuery->rowCount();
        } else {
            return NULL;
        }
    }

    public function queryOne($query, $params = null, $fetchMode = \PDO::FETCH_ASSOC){
        return $this->query($query, $params, $fetchMode, false);
    }

    /**
     * @return string
     */
    public function lastInsertId()
    {
        return $this->db->lastInsertId();
    }

    /**
     * @param $query
     * @param null $params
     * @return array
     */
    public function column($query, $params = null)
    {
        $this->init($query, $params);
        $resultColumn = $this->sQuery->fetchAll(\PDO::FETCH_COLUMN);
        $this->rowCount = $this->sQuery->rowCount();
        $this->columnCount = $this->sQuery->columnCount();
        $this->sQuery->closeCursor();
        return $resultColumn;
    }

    /**
     * @param $query
     * @param null $params
     * @param $fetchMode
     * @return mixed
     */
    public function row($query, $params = null, $fetchMode = \PDO::FETCH_ASSOC)
    {
        $this->init($query, $params);
        $resultRow = $this->sQuery->fetch($fetchMode);
        $this->rowCount = $this->sQuery->rowCount();
        $this->columnCount = $this->sQuery->columnCount();
        $this->sQuery->closeCursor();
        return $resultRow;
    }

    /**
     * @param $query
     * @param null $params
     * @return string
     */
    public function single($query, $params = null)
    {
        $this->init($query, $params);
        return $this->sQuery->fetchColumn();
    }

    /**
     * @param $message
     * @param string $sql
     * @return string
     */
    private function exceptionLog($message, $sql = "")
    {
        $exception = 'Unhandled Exception. <br />';
        $exception .= $message;
        $exception .= "<br /> You can find the error back in the log.";

        if (!empty($sql)) {
            $message .= "\r\nRaw SQL : " . $sql;
        }
        //Prevent search engines to crawl
        header("HTTP/1.1 500 Internal Server Error");
        header("Status: 500 Internal Server Error");
        return $exception;
    }
}