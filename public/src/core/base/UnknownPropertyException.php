<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\base;

class UnknownPropertyException extends \Exception
{
    /**
     * @return string the user-friendly name of this exception
     */
    public function getName()
    {
        return 'Unknown Property';
    }
}