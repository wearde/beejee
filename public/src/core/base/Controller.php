<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\base;

use amass\Base;

class Controller extends Base
{
    public $loginData = [];

    public $isGuest = true;
    /**
     * @var array|mixed|null
     */
    protected $request = [];

    /**
     * @var array|mixed|null
     */
    protected $response = [];
    
    protected $session = [];

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->loginData = self::$app['user'];
        $this->request =  $this->getInstance()->getRegistry()->get('request');
        $this->response =  $this->getInstance()->getRegistry()->get('response');
        $this->session =  $this->getInstance()->getRegistry()->get('session');

        if($this->getUid())
            $this->isGuest = false;
    }

    /**
     * @return mixed
     */
    public function getUid(){
        if(isset($this->session->data['uId'])){
            return $this->session->data['uId'];
        }
    }
}