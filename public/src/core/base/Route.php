<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\base;

use amass\Base;

class Route extends Base
{
    /**
     * @var array of routes
     */
    private $_rules = [];

    /**
     * Default Controller when empty URI
     * @var string
     */
    private $_defaultController = 'SiteController';

    /**
     * Default action for Controllers
     * @var string
     */
    private $_action = 'index';

    /**
     * Route constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;

        if(isset($config['namespace']) && $config['namespace'])
            $this->namespace = $config['namespace'];

        if(is_array($config) && isset($config['rules']))
            $this->_rules = $config['rules'];
    }

    /**
     * Returns request string
     * @return mixed
     */
    private function _getUri()
    {
        return trim($_SERVER['REQUEST_URI'], '/');
    }

    /**
     * Runs the application.
     * This is the main entrance of an application.
     */
    public function run()
    {
        $url = $this->_getUri();
        foreach ($this->_rules as $route => $rule){

            $internalRoute = preg_replace("~$route~", $rule, $url);
            $segments = explode('/', $internalRoute);
            if(preg_match("~$route~", $url))
            {
                $controllerName = ucfirst(array_shift($segments) . 'Controller');
                $actionName = 'action' . ucfirst(array_shift($segments));
                $class = $this->namespace  . 'controllers\\' . $controllerName;
                if(class_exists($class))
                {
                    $controller = new $class;
                    if(method_exists($controller, $actionName))
                    {
                        return call_user_func_array(array($controller, $actionName), $segments);
                    }
                }
            }
        }
    }
}