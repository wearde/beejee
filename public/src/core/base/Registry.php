<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace amass\core\base;

class Registry
{
    /**
     * @var array
     */
    private $_data;

    /**
     * @param $key
     * @return mixed|null
     */
    public function get($key) {
        return (isset($this->_data[$key]) ? $this->_data[$key] : null);
    }

    /**
     * @param $key
     * @param $value
     */
    public function set($key, $value) {
        $this->_data[$key] = $value;
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key) {
        return isset($this->_data[$key]);
    }
}