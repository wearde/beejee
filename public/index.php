<?php
/**
 * beejee.test
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */
ini_set('memory_limit', '400M');
defined('APP_DEBUG') or define('APP_DEBUG', true);

require(__DIR__ . '/../vendor/autoload.php');

$config = require(__DIR__ . '/../public/src/app/config/main.php');
$app = new \amass\core\base\Application($config);
$app->run();
